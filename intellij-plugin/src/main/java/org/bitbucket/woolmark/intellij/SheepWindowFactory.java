package org.bitbucket.woolmark.intellij;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowFactory;
import org.bitbucket.woolmark.swing.SheepCanvas;

import javax.swing.*;
import java.awt.*;

public class SheepWindowFactory implements ToolWindowFactory {

    @Override
    public void createToolWindowContent(Project project, ToolWindow toolWindow) {

        SheepCanvas canvas = new SheepCanvas();
        JComponent parent = toolWindow.getComponent();
        parent.add(canvas, BorderLayout.CENTER);

        toolWindow.setIcon(new ImageIcon(SheepCanvas.class.getResource("/icons/icon.png")));
        canvas.start();

    }

}
