package org.bitbucket.woolmark.swing;

import javax.swing.*;

public class Main {

    public static void main(final String... args) {

        SheepFrame frame = new SheepFrame();
        frame.setUndecorated(true);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setLocationByPlatform(true);
        frame.setVisible(true);
        frame.toFront();

    }

}
