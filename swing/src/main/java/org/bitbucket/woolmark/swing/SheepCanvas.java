package org.bitbucket.woolmark.swing;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.Random;
import java.util.ResourceBundle;

/**
 * SheepFrame Canvas.
 */
public class SheepCanvas extends JComponent implements Runnable {

    private static final long serialVersionUID = 3184839980962327198L;

    private static final int INTERACTIVE_UPDATE_RATE_MS = 100;

    private static final int DEFAULT_WIDTH = 120;

    private static final int DEFAULT_HEIGHT = 120;

    private static final int MOVE_DELTA_X = 4;

    private static final int MOVE_DELTA_Y = 3;

    private static final int JUMP_FRAME = 4;

    private static final float GROUND_HEIGHT_RATIO = 0.9f;

    private static final int COUNTER_POSITION_X = 5;

    private static final int COUNTER_POSITION_Y = 5;

    private static final String SHEEP_IMAGE_NAME00 = "/images/sheep00.png";

    private static final String SHEEP_IMAGE_NAME01 = "/images/sheep01.png";

    private static final String FENCE_IMAGE_NAME = "/images/fence.png";

    private static final String MESSAGES_NAME = "strings/messages";

    private static final Color SKY_COLOR = new Color(150, 150, 255);

    private static final Color GROUND_COLOR = new Color(100, 255, 100);

    private static final Random rand = new Random();

    private Image[] sheepImages;

    private Image fenceImage;

    private java.util.List<int[]> sheepFlock = new LinkedList<>();

    private int scale = 1;

    private int groundHeight;

    private boolean stretch;

    private int sheepCount;

    private boolean sheepAppend;

    private boolean running;

    private String counterFormat;

    public SheepCanvas() {
        super();
        setPreferredSize(new Dimension(DEFAULT_WIDTH, DEFAULT_HEIGHT));

        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                super.componentResized(e);

                System.out.println("resized");
                scale = getPreferredScale(getWidth(), getHeight());
                groundHeight = (int) ((float) (fenceImage.getHeight(null) * scale) * GROUND_HEIGHT_RATIO);
                sheepFlock = new LinkedList<>();
            }
        });
        addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);
                sheepAppend = true;
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                super.mouseReleased(e);
                sheepAppend = false;
            }
        });

        sheepImages = new Image[2];
        sheepImages[0] = readImage(SHEEP_IMAGE_NAME00);
        sheepImages[1] = readImage(SHEEP_IMAGE_NAME01);
        fenceImage = readImage(FENCE_IMAGE_NAME);
        counterFormat = ResourceBundle.getBundle(MESSAGES_NAME).getString("counter");
    }

    @Override
    public synchronized void paintComponent(final Graphics g) {
        super.paintComponent(g);

        // draw sky and ground
        g.setColor(SKY_COLOR);
        g.fillRect(0, 0, getWidth(), getHeight());
        g.setColor(GROUND_COLOR);
        g.fillRect(0, getHeight() - groundHeight, getWidth(), groundHeight);

        // draw a fence
        g.drawImage(fenceImage,
                (getWidth() - fenceImage.getWidth(null) * scale) / 2,
                getHeight() - fenceImage.getHeight(null) * scale,
                fenceImage.getWidth(null) * scale,
                fenceImage.getHeight(null) * scale,
                null);

        // draw all sheepFlock
        for (int[] sheep : sheepFlock) {
            g.drawImage(sheepImages[(sheep[2] == 0 || stretch) ? 1 : 0],
                    sheep[0], sheep[1],
                    sheepImages[0].getWidth(null) * scale,
                    sheepImages[0].getHeight(null) * scale,
                    null);
        }

        // draw counter
        g.setColor(Color.BLACK);
        g.drawString(String.format(counterFormat, sheepCount),
                COUNTER_POSITION_X,
                COUNTER_POSITION_Y + g.getFont().getSize());

    }

    @Override
    public void run() {
        running = true;

        long lastSleepTime = System.currentTimeMillis();

        while (running) {
            update();

            try {
                Thread.sleep(INTERACTIVE_UPDATE_RATE_MS - (System.currentTimeMillis() - lastSleepTime));
            } catch (InterruptedException e) {
                // Ignore interrupted exception
            }

            lastSleepTime = System.currentTimeMillis();
        }
    }

    public void update() {
        calc();
        repaint();
    }

    public synchronized void calc() {

        // add sheepFlock
        if (groundHeight > 0 && (sheepFlock.size() <= 0 || sheepAppend)) {
            sheepFlock.add(newSheep());
        }

        java.util.List<int[]> newSheepFlock = new LinkedList<int[]>();
        for (int[] sheep : sheepFlock) {

            sheep[0] -= MOVE_DELTA_X * scale;

            // check entering jump position
            if (sheep[2] < 0 && sheep[3] <= sheep[0]
                    && sheep[0] < sheep[3] + MOVE_DELTA_X * scale) {
                sheep[2] = 0;
            }

            // jump
            if (sheep[2] >= 0) {

                if (sheep[2] < JUMP_FRAME) {
                    sheep[1] -= MOVE_DELTA_Y * scale;
                } else if (sheep[2] < JUMP_FRAME * 2) {
                    sheep[1] += MOVE_DELTA_Y * scale;
                } else {
                    sheep[2] = Integer.MIN_VALUE;
                }

                sheep[2]++;
            }

            // count sheepFlock
            if (sheep[2] == JUMP_FRAME) {
                sheepCount++;
            }

            // go away
            if (sheep[0] >= -sheepImages[0].getWidth(null) * scale) {
                newSheepFlock.add(sheep);
            }

        }

        sheepFlock = newSheepFlock;
        stretch = !stretch;

    }

    public void start() {
        if (running) {
            return;
        }

        // load number
        sheepCount = readInt(null);

        // start main loop thread
        Thread runner = new Thread(this);
        runner.start();
    }

    public void stop() {
//        running = false;
//
//        // save number
//        writeInt(null);
    }

    private Image readImage(final String name) {

        Image image = null;
        InputStream is = null;
        try {
            is = SheepCanvas.class.getResourceAsStream(name);
            image = ImageIO.read(is);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    System.out.println(e.toString());
                }
            }
        }

        return image;
    }

    private int[] newSheep() {

        int[] sheep = new int[4];
        sheep[0] = getWidth() + sheepImages[0].getWidth(null) * scale;
        sheep[1] = getHeight() - groundHeight
                + rand.nextInt(groundHeight - sheepImages[0].getHeight(null) * scale);
        sheep[2] = Integer.MIN_VALUE;
        sheep[3] = calcJumpX(sheep[1]);

        return sheep;
    }

    private int calcJumpX(final int y) {
        // y = -1 * fence.height / fence.width * (x - (width - fence.width) / 2) + height

        int fw = getWidth();
        int fh = getHeight();

        int w = fenceImage.getWidth(null) * scale;
        int h = fenceImage.getHeight(null) * scale;

        return -1 * (y - fh) * w / h + (fw - w) / 2;

    }

    private int getPreferredScale(final int width, final int height) {

        int scale;

        if (width < height) {
            scale = width / DEFAULT_WIDTH;
        } else {
            scale = height / DEFAULT_HEIGHT;
        }

        if (scale < 1) {
            scale = 1;
        }

        return scale;
    }

    private void writeInt(final String file) {
        // XXX: write sheepFlock number from file
    }

    private int readInt(final String file) {
        int number = 0;
        // XXX: read sheepFlock number from file
        return number;
    }

}
